# README #

### What is this repository for? ###

* A link to a Tracking and Contact Tracing Application built in Google Sheets
* In the future this could also hold some utility scripts for ETL processes
* 0.1.0
* See https://drive.google.com/drive/folders/1n8kqVGijX4ynpolxxkirwpjC2aDllYC8 for all the details
* Please note - ***this was written using Google Sheets and Appscript, do not use it for very large scale operations!***

### Contribution guidelines ###

* Writing tests - yes it could use these
* Code review - I wish we could,  but its written in appscript

### Who do I talk to? ###

* brad<dot>nemetski<at>gmail<dot>com